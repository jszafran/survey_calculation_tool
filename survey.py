import pandas as pd
import csv
import time
from functools import reduce

def _get_parents_list_from_node(node):
    """
    Method for retrieving list of parents from node provided, i.e:
    N01.05.04.02. -> [ 'N01.05.04.', 'N01.05.', N01.' ]
    """
    a = []
    n = node.split(".")
    del n[-2:]
    if len(n) == 0:
        return a
    a.append(n.pop(0) + ".")
    if len(n) > 0:
        for i in range(0, len(n)):
            a.append(a[-1] + n[i] + ".")
    return a


def get_units_directly_under_root(nodes_list):
    lst = [n.split(".") for n in nodes_list]
    for el in lst:
        del el[-1]
    lst = list(filter(lambda x: len(x) == 1, lst))
    if len(lst) == 0:
        return []
    return [x[0] + "." for x in lst]


def generate_questions_list(start, end, prefix="qst"):
    """
    Method for generating list of sequencial question names.
    Prefix set to 'qst' by default - i.e. 'qst1' , 'qst2', 'qst3' etc.
    """
    if isinstance(start, int) and isinstance(end, int):
        if start <= end:
            return ["{}{}".format(prefix, i) for i in range(start, end + 1)]
        return "Start value should smaller or equal to end value!"
    return "Start and end value should be integers!"


class QuestionScaleMapping:
    def __init__(self, qsts_amount=0, mapping=None):
        self.qsts_amount = qsts_amount
        if mapping == None:
            self.mapping = {}
        else:
            self.mapping = mapping

    def __repr__(self):
        return self.mapping

    def create_default_mapping(
        self, def_val, qsts_list, exceptions=None, exception_scales=None
    ):
        if not def_val:
            return "Please enter default value!"
        d = {}
        for qst in qsts_list:
            d[qst] = def_val

        if exceptions and exception_scales:
            if len(exceptions) == len(exception_scales):
                for i in range(0, len(exceptions)):
                    d[exceptions[i]] = exception_scales[i]
        self.mapping = d

    def set_qst_scale(self, qst, val):
        if qst not in self.mapping:
            self.mapping[qst] = val
            return "Set mapping of question {} to value {}".format(qst, val)
        if self.mapping[qst] == val:
            return "You're trying to assign {} mapping to current value ({}).".format(
                qst, val
            )
        old_val = self.mapping[qst]
        self.mapping[qst] = val
        return "Changed mapping of question {} from {} to {} value!".format(
            qst, old_val, val
        )


class DataCut:
    def __init__(self, **kwargs):
        self.name = kwargs["name"]
        self.node = kwargs["node"]
        self.demogs = kwargs["demogs"]

    def __repr__(self):
        return "Name: {}, Node: {}, demogs: {}".format(
            self.name, self.node, self.demogs
        )


class Survey:
    def __init__(self, **kwargs):
        if "path" in kwargs:
            try:
                self.df = pd.read_csv(kwargs["path"])
                # nodes have to sorted for using
                # optimized index feature
                self.df = self.df.sort_values("org_node")
            except:
                print("An error occured while loading the data from csv!")
                self.df = None
        else:
            self.df = None
        self.cuts_list = []
        self.name = kwargs["name"]
        self.qsts_amount = kwargs["qsts_amount"]
        self.mapping = kwargs["mapping"]
        if "nodes_list" in kwargs:
            try:
                self.nodes_list = pd.read_csv(kwargs["nodes_list"])
                # nodes have to be sorted for using
                # optimized index feature
                # $ self.nodes_list = self.nodes_list.sort_values("org_node")
            except:
                print("An error occured while loading the data from csv!")
                self.nodes_list = None

    def __get_cuts_from_csv(self, **kwargs):
        with open(kwargs["path"], "r") as csvfile:
            reader = csv.reader(csvfile, delimiter=",")

            if kwargs["header"]:
                next(reader)

            for row in reader:
                d = {}
                i = 0
                for demog in row[3:]:
                    i += 1
                    if demog != "":
                        d.update({"d{}".format(i): demog})
                self.cuts_list.append(DataCut(name=row[1], node=row[2], demogs=d))

    def load_cuts(self, path, header):
        self.__get_cuts_from_csv(path=path, header=header)

    def load_mapping(self, mapping_obj):
        if isinstance(mapping_obj, QuestionScaleMapping):
            self.mapping = mapping_obj.mapping

    def create_optimized_search_index(self):
        """
        This method will create a data structure which will speed up
        filtering respondents by organizational unit.
        Example of index data structure:
        { 'N01.01.': [ 12, 62, 12, 15 ] } 
         unit_key:   start,end,start,end
                       rollups  directs     
        """
        self.optimized_search_index = {}
        respondents_units = pd.Series(self.df["org_node"]).tolist()
        org_units = self.nodes_list["org_node"].tolist()
        last_found_index = 0
        end_of_list = len(respondents_units)

        t1 = time.time()
        # loading direct respondents indexes
        for unit in org_units:
            if not unit in respondents_units:
                self.optimized_search_index[unit] = [-1, -1, -1, -1]
            else:
                i = respondents_units.index(unit, last_found_index, end_of_list)
                amount = respondents_units.count(unit)
                self.optimized_search_index[unit] = [
                    i,
                    i + amount - 1,
                    i,
                    i + amount - 1,
                ]
                last_found_index = i + amount - 1

        t1 = time.time()
        # creating rollups
        for idx, unit in enumerate(org_units):
            last_indexed_child = unit
            tmp_idx = idx + 1
            # check if next in-line unit is a child of current unit
            while tmp_idx < len(org_units) and org_units[tmp_idx][: len(unit)] == unit:
                last_indexed_child = org_units[tmp_idx]
                tmp_idx += 1
            self.optimized_search_index[unit][1] = self.optimized_search_index[
                last_indexed_child
            ][3]
            # print('Unit {} - last child: {}'.format(unit,last_indexed_child))
        t2 = time.time()

        under_root_units = get_units_directly_under_root(org_units)
        print("under roots are:")
        print(under_root_units)
        # assign starting point for units located directly under root (under_root_units)
        for unit in under_root_units:
            loc = org_units.index(unit)
            while loc < len(org_units) - 2:
                if (
                    self.optimized_search_index[org_units[loc + 1]][0] != -1
                    and self.optimized_search_index[unit][0] == -1
                ):
                    self.optimized_search_index[unit][0] = self.optimized_search_index[
                        org_units[loc + 1]
                    ][0]
                    break
                loc += 1

        for unit in self.optimized_search_index:
            if (
                self.optimized_search_index[unit][0]
                * self.optimized_search_index[unit][1]
                <= 0
            ):
                children = _get_parents_list_from_node(unit)
                for child in children:
                    if self.optimized_search_index[child][0] != -1:
                        self.optimized_search_index[unit][
                            0
                        ] = self.optimized_search_index[child][0]
                        break

        print("It took {} s to rollup respondents.".format(t2 - t1))


    def _get_bool_mask_by_demog_filter(self, demog, filter_value):
        return (self.df[demog] == filter_value)

    def _get_bool_mask_by_node_filter(self, node):
        return self.df['org_node'].str.contains(node, regex=False)


    def _multiply_demog_masks(self, d):
        l = []
        for key in d:
            l.append(self._get_bool_mask_by_demog_filter(key, d[key]))
        return reduce(lambda x, y: x & y, l)

    def filter_subset_by_cut(self, cut):
        if len(cut.demogs) == 0:
            return self._get_bool_mask_by_node_filter(cut.node)
        return self._get_bool_mask_by_node_filter(cut.node) & self._multiply_demog_masks(cut.demogs)


    def get_question_answers_occurences(self, q):
        pass

    def test_arrays(self):
        pass